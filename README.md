# xivo-unixodbc-packaging

Debian packaging for unixodbc used in XiVO.

Forked from the official Debian unixodbc packaging on 2016-05-30. A newer
upstream version of unixodbc was required in XiVO since the 2.3.1 version
provided by Debian was otherwise causing Asterisk 13.8 and later to crash.
